﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKAvocats.Models
{
    public class Contract
    {
        public int Id { get; set; }
        public string TemplateName { get; set; }
        public List<Client> Clients { get; set; }
    }
}
