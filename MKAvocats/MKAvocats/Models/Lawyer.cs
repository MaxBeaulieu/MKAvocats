﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKAvocats.Models
{
    public class Lawyer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public LawyerRank Rank { get; set; }
    }

    public enum LawyerRank 
    { 
        Junior = 0, 
        Lawyer = 1, 
        Senior = 2
    }
}
