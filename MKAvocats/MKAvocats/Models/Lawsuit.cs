﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKAvocats.Models
{
    public class Lawsuit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public List<Lawyer> Lawyers { get; set; }
        public List<Client> Clients { get; set; }
    }
}
